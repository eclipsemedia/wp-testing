# WordPress MySQL database migration
#
# Generated: Wednesday 27. May 2020 20:26 UTC
# Hostname: db
# Database: `default`
# URL: //wp-testing.eclipse-dev.com
# Path: /var/www/docroot
# Tables: wp_commentmeta, wp_comments, wp_links, wp_options, wp_postmeta, wp_posts, wp_term_relationships, wp_term_taxonomy, wp_termmeta, wp_terms, wp_usermeta, wp_users
# Table Prefix: wp_
# Post Types: revision, acf-field, acf-field-group, attachment, block_lab, page, post, report, wp_block, wpcf7_contact_form
# Protocol: http
# Multisite: false
# Subsite Export: false
# --------------------------------------------------------

/*!40101 SET NAMES utf8mb4 */;

SET sql_mode='NO_AUTO_VALUE_ON_ZERO';



#
# Delete any existing table `wp_commentmeta`
#

DROP TABLE IF EXISTS `wp_commentmeta`;


#
# Table structure of table `wp_commentmeta`
#

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_commentmeta`
#

#
# End of data contents of table `wp_commentmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_comments`
#

DROP TABLE IF EXISTS `wp_comments`;


#
# Table structure of table `wp_comments`
#

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_comments`
#
INSERT INTO `wp_comments` ( `comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2020-04-30 18:44:11', '2020-04-30 18:44:11', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href="https://gravatar.com">Gravatar</a>.', 0, '1', '', '', 0, 0) ;

#
# End of data contents of table `wp_comments`
# --------------------------------------------------------



#
# Delete any existing table `wp_links`
#

DROP TABLE IF EXISTS `wp_links`;


#
# Table structure of table `wp_links`
#

CREATE TABLE `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_links`
#

#
# End of data contents of table `wp_links`
# --------------------------------------------------------



#
# Delete any existing table `wp_options`
#

DROP TABLE IF EXISTS `wp_options`;


#
# Table structure of table `wp_options`
#

CREATE TABLE `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`),
  KEY `autoload` (`autoload`)
) ENGINE=InnoDB AUTO_INCREMENT=310 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_options`
#
INSERT INTO `wp_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://wp-testing.eclipse-dev.com', 'yes'),
(2, 'home', 'http://wp-testing.eclipse-dev.com', 'yes'),
(3, 'blogname', 'wp-testing', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'kyle@eclipsemediasolutions.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:126:{s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:21:"^index.php/wp-json/?$";s:22:"index.php?rest_route=/";s:24:"^index.php/wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:9:"report/?$";s:26:"index.php?post_type=report";s:39:"report/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?post_type=report&feed=$matches[1]";s:34:"report/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?post_type=report&feed=$matches[1]";s:26:"report/page/([0-9]{1,})/?$";s:44:"index.php?post_type=report&paged=$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:35:"block_lab/.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:45:"block_lab/.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:65:"block_lab/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:60:"block_lab/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:60:"block_lab/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:41:"block_lab/.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:24:"block_lab/(.+?)/embed/?$";s:42:"index.php?block_lab=$matches[1]&embed=true";s:28:"block_lab/(.+?)/trackback/?$";s:36:"index.php?block_lab=$matches[1]&tb=1";s:36:"block_lab/(.+?)/page/?([0-9]{1,})/?$";s:49:"index.php?block_lab=$matches[1]&paged=$matches[2]";s:43:"block_lab/(.+?)/comment-page-([0-9]{1,})/?$";s:49:"index.php?block_lab=$matches[1]&cpage=$matches[2]";s:32:"block_lab/(.+?)(?:/([0-9]+))?/?$";s:48:"index.php?block_lab=$matches[1]&page=$matches[2]";s:34:"report/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:44:"report/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:64:"report/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"report/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"report/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:40:"report/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:23:"report/([^/]+)/embed/?$";s:39:"index.php?report=$matches[1]&embed=true";s:27:"report/([^/]+)/trackback/?$";s:33:"index.php?report=$matches[1]&tb=1";s:47:"report/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?report=$matches[1]&feed=$matches[2]";s:42:"report/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?report=$matches[1]&feed=$matches[2]";s:35:"report/([^/]+)/page/?([0-9]{1,})/?$";s:46:"index.php?report=$matches[1]&paged=$matches[2]";s:42:"report/([^/]+)/comment-page-([0-9]{1,})/?$";s:46:"index.php?report=$matches[1]&cpage=$matches[2]";s:31:"report/([^/]+)(?:/([0-9]+))?/?$";s:45:"index.php?report=$matches[1]&page=$matches[2]";s:23:"report/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:33:"report/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:53:"report/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:48:"report/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:48:"report/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:29:"report/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:12:"robots\\.txt$";s:18:"index.php?robots=1";s:13:"favicon\\.ico$";s:19:"index.php?favicon=1";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:27:"comment-page-([0-9]{1,})/?$";s:38:"index.php?&page_id=2&cpage=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:58:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:68:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:88:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:83:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:83:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:64:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:53:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$";s:91:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$";s:85:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1";s:77:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]";s:72:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]";s:65:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$";s:98:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]";s:72:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$";s:98:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]";s:61:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]";s:47:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:57:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:77:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:72:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:72:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:53:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]";s:51:"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]";s:38:"([0-9]{4})/comment-page-([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&cpage=$matches[2]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:5:{i:0;s:30:"advanced-custom-fields/acf.php";i:1;s:23:"block-lab/block-lab.php";i:2;s:36:"contact-form-7/wp-contact-form-7.php";i:3;s:9:"hello.php";i:4;s:31:"wp-migrate-db/wp-migrate-db.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:2:{i:0;s:57:"/var/www/docroot/wp-content/themes/twentytwenty/style.css";i:2;s:0:"";}', 'no'),
(40, 'template', 'generatepress', 'yes'),
(41, 'stylesheet', 'gp-child', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '47018', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '2', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'admin_email_lifespan', '1603824247', 'yes'),
(94, 'initial_db_version', '47018', 'yes'),
(95, 'wp_user_roles', 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:68:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:20:"block_lab_edit_block";b:1;s:21:"block_lab_edit_blocks";b:1;s:28:"block_lab_edit_others_blocks";b:1;s:24:"block_lab_publish_blocks";b:1;s:20:"block_lab_read_block";b:1;s:29:"block_lab_read_private_blocks";b:1;s:22:"block_lab_delete_block";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}}', 'yes'),
(96, 'fresh_site', '0', 'yes'),
(97, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(99, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(100, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes') ;
INSERT INTO `wp_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(101, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:12:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:3:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";}s:9:"sidebar-2";a:3:{i:0;s:10:"archives-2";i:1;s:12:"categories-2";i:2;s:6:"meta-2";}s:6:"header";a:0:{}s:8:"footer-1";a:0:{}s:8:"footer-2";a:0:{}s:8:"footer-3";a:0:{}s:8:"footer-4";a:0:{}s:8:"footer-5";a:0:{}s:10:"footer-bar";a:0:{}s:7:"top-bar";a:0:{}s:13:"array_version";i:3;}', 'yes'),
(103, 'cron', 'a:7:{i:1590612253;a:1:{s:34:"wp_privacy_delete_old_export_files";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:6:"hourly";s:4:"args";a:0:{}s:8:"interval";i:3600;}}}i:1590648253;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1590691452;a:1:{s:32:"recovery_mode_clean_expired_keys";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1590691473;a:2:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}s:25:"delete_expired_transients";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1590691474;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1590777852;a:1:{s:30:"wp_site_health_scheduled_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:6:"weekly";s:4:"args";a:0:{}s:8:"interval";i:604800;}}}s:7:"version";i:2;}', 'yes'),
(104, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(105, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(106, 'widget_media_audio', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(107, 'widget_media_image', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(108, 'widget_media_gallery', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(109, 'widget_media_video', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(110, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(111, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(112, 'widget_custom_html', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(114, 'theme_mods_twentytwenty', 'a:2:{s:18:"custom_css_post_id";i:-1;s:16:"sidebars_widgets";a:2:{s:4:"time";i:1588623344;s:4:"data";a:3:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:3:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";}s:9:"sidebar-2";a:3:{i:0;s:10:"archives-2";i:1;s:12:"categories-2";i:2;s:6:"meta-2";}}}}', 'yes'),
(115, 'recovery_keys', 'a:0:{}', 'yes'),
(127, 'can_compress_scripts', '1', 'no'),
(140, 'recently_activated', 'a:0:{}', 'yes'),
(145, 'block_lab_example_post_id', '5', 'yes'),
(217, 'current_theme', 'wp theme Child', 'yes'),
(218, 'theme_mods_twentytwentychild', 'a:4:{i:0;b:0;s:18:"nav_menu_locations";a:0:{}s:18:"custom_css_post_id";i:-1;s:16:"sidebars_widgets";a:2:{s:4:"time";i:1588635122;s:4:"data";a:3:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:3:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";}s:9:"sidebar-2";a:3:{i:0;s:10:"archives-2";i:1;s:12:"categories-2";i:2;s:6:"meta-2";}}}}', 'yes'),
(219, 'theme_switched', '', 'yes'),
(225, 'generate_settings', 'a:42:{s:10:"hide_title";s:0:"";s:12:"hide_tagline";s:0:"";s:4:"logo";s:0:"";s:25:"inline_logo_site_branding";b:0;s:11:"retina_logo";s:0:"";s:10:"logo_width";s:0:"";s:13:"top_bar_width";s:4:"full";s:19:"top_bar_inner_width";s:9:"contained";s:17:"top_bar_alignment";s:5:"right";s:15:"container_width";s:4:"1100";s:19:"container_alignment";s:5:"boxes";s:21:"header_layout_setting";s:12:"fluid-header";s:18:"header_inner_width";s:9:"contained";s:21:"nav_alignment_setting";s:4:"left";s:24:"header_alignment_setting";s:4:"left";s:18:"nav_layout_setting";s:9:"fluid-nav";s:15:"nav_inner_width";s:9:"contained";s:20:"nav_position_setting";s:16:"nav-below-header";s:14:"nav_drop_point";s:0:"";s:17:"nav_dropdown_type";s:5:"hover";s:22:"nav_dropdown_direction";s:5:"right";s:10:"nav_search";s:7:"disable";s:22:"content_layout_setting";s:19:"separate-containers";s:14:"layout_setting";s:13:"right-sidebar";s:19:"blog_layout_setting";s:13:"right-sidebar";s:21:"single_layout_setting";s:13:"right-sidebar";s:12:"post_content";s:4:"full";s:21:"footer_layout_setting";s:12:"fluid-footer";s:18:"footer_inner_width";s:9:"contained";s:21:"footer_widget_setting";s:1:"3";s:20:"footer_bar_alignment";s:5:"right";s:11:"back_to_top";s:0:"";s:16:"background_color";s:7:"#efefef";s:10:"text_color";s:7:"#3a3a3a";s:10:"link_color";s:7:"#1e73be";s:16:"link_color_hover";s:7:"#000000";s:18:"link_color_visited";s:0:"";s:23:"font_awesome_essentials";b:0;s:5:"icons";s:4:"font";s:11:"combine_css";b:0;s:17:"dynamic_css_cache";b:0;s:9:"font_body";s:9:"Open Sans";}', 'yes'),
(226, 'generate_migration_settings', 'a:5:{s:11:"combine_css";s:4:"done";s:31:"font_awesome_essentials_updated";s:4:"true";s:22:"skip_dynamic_css_cache";s:4:"true";s:20:"default_font_updated";s:4:"true";s:25:"blog_post_content_preview";s:4:"true";}', 'yes'),
(227, 'generate_dynamic_css_output', 'body{background-color:#efefef;color:#3a3a3a;}a, a:visited{color:#1e73be;}a:hover, a:focus, a:active{color:#000000;}body .grid-container{max-width:1100px;}.wp-block-group__inner-container{max-width:1100px;margin-left:auto;margin-right:auto;}body, button, input, select, textarea{font-family:-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";}.entry-content > [class*="wp-block-"]:not(:last-child){margin-bottom:1.5em;}.main-navigation .main-nav ul ul li a{font-size:14px;}@media (max-width:768px){.main-title{font-size:30px;}h1{font-size:30px;}h2{font-size:25px;}}.top-bar{background-color:#636363;color:#ffffff;}.top-bar a,.top-bar a:visited{color:#ffffff;}.top-bar a:hover{color:#303030;}.site-header{background-color:#ffffff;color:#3a3a3a;}.site-header a,.site-header a:visited{color:#3a3a3a;}.main-title a,.main-title a:hover,.main-title a:visited{color:#222222;}.site-description{color:#757575;}.main-navigation,.main-navigation ul ul{background-color:#222222;}.main-navigation .main-nav ul li a,.menu-toggle{color:#ffffff;}.main-navigation .main-nav ul li:hover > a,.main-navigation .main-nav ul li:focus > a, .main-navigation .main-nav ul li.sfHover > a{color:#ffffff;background-color:#3f3f3f;}button.menu-toggle:hover,button.menu-toggle:focus,.main-navigation .mobile-bar-items a,.main-navigation .mobile-bar-items a:hover,.main-navigation .mobile-bar-items a:focus{color:#ffffff;}.main-navigation .main-nav ul li[class*="current-menu-"] > a{color:#ffffff;background-color:#3f3f3f;}.main-navigation .main-nav ul li[class*="current-menu-"] > a:hover,.main-navigation .main-nav ul li[class*="current-menu-"].sfHover > a{color:#ffffff;background-color:#3f3f3f;}.navigation-search input[type="search"],.navigation-search input[type="search"]:active, .navigation-search input[type="search"]:focus, .main-navigation .main-nav ul li.search-item.active > a{color:#ffffff;background-color:#3f3f3f;}.main-navigation ul ul{background-color:#3f3f3f;}.main-navigation .main-nav ul ul li a{color:#ffffff;}.main-navigation .main-nav ul ul li:hover > a,.main-navigation .main-nav ul ul li:focus > a,.main-navigation .main-nav ul ul li.sfHover > a{color:#ffffff;background-color:#4f4f4f;}.main-navigation .main-nav ul ul li[class*="current-menu-"] > a{color:#ffffff;background-color:#4f4f4f;}.main-navigation .main-nav ul ul li[class*="current-menu-"] > a:hover,.main-navigation .main-nav ul ul li[class*="current-menu-"].sfHover > a{color:#ffffff;background-color:#4f4f4f;}.separate-containers .inside-article, .separate-containers .comments-area, .separate-containers .page-header, .one-container .container, .separate-containers .paging-navigation, .inside-page-header{background-color:#ffffff;}.entry-meta{color:#595959;}.entry-meta a,.entry-meta a:visited{color:#595959;}.entry-meta a:hover{color:#1e73be;}.sidebar .widget{background-color:#ffffff;}.sidebar .widget .widget-title{color:#000000;}.footer-widgets{background-color:#ffffff;}.footer-widgets .widget-title{color:#000000;}.site-info{color:#ffffff;background-color:#222222;}.site-info a,.site-info a:visited{color:#ffffff;}.site-info a:hover{color:#606060;}.footer-bar .widget_nav_menu .current-menu-item a{color:#606060;}input[type="text"],input[type="email"],input[type="url"],input[type="password"],input[type="search"],input[type="tel"],input[type="number"],textarea,select{color:#666666;background-color:#fafafa;border-color:#cccccc;}input[type="text"]:focus,input[type="email"]:focus,input[type="url"]:focus,input[type="password"]:focus,input[type="search"]:focus,input[type="tel"]:focus,input[type="number"]:focus,textarea:focus,select:focus{color:#666666;background-color:#ffffff;border-color:#bfbfbf;}button,html input[type="button"],input[type="reset"],input[type="submit"],a.button,a.button:visited,a.wp-block-button__link:not(.has-background){color:#ffffff;background-color:#666666;}button:hover,html input[type="button"]:hover,input[type="reset"]:hover,input[type="submit"]:hover,a.button:hover,button:focus,html input[type="button"]:focus,input[type="reset"]:focus,input[type="submit"]:focus,a.button:focus,a.wp-block-button__link:not(.has-background):active,a.wp-block-button__link:not(.has-background):focus,a.wp-block-button__link:not(.has-background):hover{color:#ffffff;background-color:#3f3f3f;}.generate-back-to-top,.generate-back-to-top:visited{background-color:rgba( 0,0,0,0.4 );color:#ffffff;}.generate-back-to-top:hover,.generate-back-to-top:focus{background-color:rgba( 0,0,0,0.6 );color:#ffffff;}.entry-content .alignwide, body:not(.no-sidebar) .entry-content .alignfull{margin-left:-40px;width:calc(100% + 80px);max-width:calc(100% + 80px);}.rtl .menu-item-has-children .dropdown-menu-toggle{padding-left:20px;}.rtl .main-navigation .main-nav ul li.menu-item-has-children > a{padding-right:20px;}@media (max-width:768px){.separate-containers .inside-article, .separate-containers .comments-area, .separate-containers .page-header, .separate-containers .paging-navigation, .one-container .site-content, .inside-page-header, .wp-block-group__inner-container{padding:30px;}.entry-content .alignwide, body:not(.no-sidebar) .entry-content .alignfull{margin-left:-30px;width:calc(100% + 60px);max-width:calc(100% + 60px);}}.one-container .sidebar .widget{padding:0px;}', 'yes'),
(228, 'generate_dynamic_css_cached_version', '2.4.2', 'yes'),
(229, 'theme_mods_generatepress', 'a:5:{s:18:"nav_menu_locations";a:0:{}s:18:"custom_css_post_id";i:-1;s:18:"font_body_category";s:10:"sans-serif";s:18:"font_body_variants";s:70:"300,300italic,regular,italic,600,600italic,700,700italic,800,800italic";s:16:"sidebars_widgets";a:2:{s:4:"time";i:1588636043;s:4:"data";a:11:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:3:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";}s:9:"sidebar-2";a:3:{i:0;s:10:"archives-2";i:1;s:12:"categories-2";i:2;s:6:"meta-2";}s:6:"header";a:0:{}s:8:"footer-1";a:0:{}s:8:"footer-2";a:0:{}s:8:"footer-3";a:0:{}s:8:"footer-4";a:0:{}s:8:"footer-5";a:0:{}s:10:"footer-bar";a:0:{}s:7:"top-bar";a:0:{}}}}', 'yes'),
(232, 'generate_db_version', '2.4.2', 'no'),
(233, 'generate_update_core_typography', 'true', 'yes'),
(235, 'theme_mods_gp-child', 'a:5:{i:0;b:0;s:18:"nav_menu_locations";a:0:{}s:18:"font_body_category";s:10:"sans-serif";s:18:"font_body_variants";s:70:"300,300italic,regular,italic,600,600italic,700,700italic,800,800italic";s:18:"custom_css_post_id";i:-1;}', 'yes'),
(256, 'acf_version', '5.8.9', 'yes'),
(299, 'wpcf7', 'a:2:{s:7:"version";s:5:"5.1.9";s:13:"bulk_validate";a:4:{s:9:"timestamp";i:1590606228;s:7:"version";s:5:"5.1.9";s:11:"count_valid";i:1;s:13:"count_invalid";i:0;}}', 'yes'),
(307, 'wpmdb_usage', 'a:2:{s:6:"action";s:8:"savefile";s:4:"time";i:1590611161;}', 'no') ;

#
# End of data contents of table `wp_options`
# --------------------------------------------------------



#
# Delete any existing table `wp_postmeta`
#

DROP TABLE IF EXISTS `wp_postmeta`;


#
# Table structure of table `wp_postmeta`
#

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_postmeta`
#
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 6, '_edit_last', '1'),
(4, 6, '_edit_lock', '1588289518:1'),
(5, 1, '_edit_lock', '1588287673:1'),
(6, 8, '_wp_attached_file', '2020/04/lilac.jpg'),
(7, 8, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1500;s:6:"height";i:2000;s:4:"file";s:17:"2020/04/lilac.jpg";s:5:"sizes";a:6:{s:6:"medium";a:4:{s:4:"file";s:17:"lilac-225x300.jpg";s:5:"width";i:225;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:18:"lilac-768x1024.jpg";s:5:"width";i:768;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:17:"lilac-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:18:"lilac-768x1024.jpg";s:5:"width";i:768;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}s:9:"1536x1536";a:4:{s:4:"file";s:19:"lilac-1152x1536.jpg";s:5:"width";i:1152;s:6:"height";i:1536;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:19:"lilac-1200x1600.jpg";s:5:"width";i:1200;s:6:"height";i:1600;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(10, 1, '_wp_page_template', 'templates/template-full-width.php'),
(11, 16, '_edit_last', '1'),
(12, 16, '_edit_lock', '1590606265:1'),
(13, 17, '_wp_attached_file', '2020/04/bicycle.jpg'),
(14, 17, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1200;s:6:"height";i:1680;s:4:"file";s:19:"2020/04/bicycle.jpg";s:5:"sizes";a:5:{s:6:"medium";a:4:{s:4:"file";s:19:"bicycle-214x300.jpg";s:5:"width";i:214;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"bicycle-731x1024.jpg";s:5:"width";i:731;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:19:"bicycle-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"bicycle-768x1075.jpg";s:5:"width";i:768;s:6:"height";i:1075;s:9:"mime-type";s:10:"image/jpeg";}s:9:"1536x1536";a:4:{s:4:"file";s:21:"bicycle-1097x1536.jpg";s:5:"width";i:1097;s:6:"height";i:1536;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(18, 22, '_wp_attached_file', '2020/04/colors.jpg'),
(19, 22, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1800;s:6:"height";i:1200;s:4:"file";s:18:"2020/04/colors.jpg";s:5:"sizes";a:6:{s:6:"medium";a:4:{s:4:"file";s:18:"colors-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:19:"colors-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:18:"colors-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:18:"colors-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:9:"1536x1536";a:4:{s:4:"file";s:20:"colors-1536x1024.jpg";s:5:"width";i:1536;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:19:"colors-1200x800.jpg";s:5:"width";i:1200;s:6:"height";i:800;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(24, 2, '_edit_lock', '1590609121:1'),
(25, 29, '_edit_lock', '1588288939:1'),
(27, 32, '_wp_attached_file', '2020/04/hasin-farhan-7J-4ffEyF0Y-unsplash1-scaled.jpg'),
(28, 32, '_wp_attachment_metadata', 'a:6:{s:5:"width";i:2560;s:6:"height";i:1708;s:4:"file";s:53:"2020/04/hasin-farhan-7J-4ffEyF0Y-unsplash1-scaled.jpg";s:5:"sizes";a:8:{s:6:"medium";a:4:{s:4:"file";s:46:"hasin-farhan-7J-4ffEyF0Y-unsplash1-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:47:"hasin-farhan-7J-4ffEyF0Y-unsplash1-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:46:"hasin-farhan-7J-4ffEyF0Y-unsplash1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:46:"hasin-farhan-7J-4ffEyF0Y-unsplash1-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:9:"1536x1536";a:4:{s:4:"file";s:48:"hasin-farhan-7J-4ffEyF0Y-unsplash1-1536x1025.jpg";s:5:"width";i:1536;s:6:"height";i:1025;s:9:"mime-type";s:10:"image/jpeg";}s:9:"2048x2048";a:4:{s:4:"file";s:48:"hasin-farhan-7J-4ffEyF0Y-unsplash1-2048x1366.jpg";s:5:"width";i:2048;s:6:"height";i:1366;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:47:"hasin-farhan-7J-4ffEyF0Y-unsplash1-1200x800.jpg";s:5:"width";i:1200;s:6:"height";i:800;s:9:"mime-type";s:10:"image/jpeg";}s:23:"twentytwenty-fullscreen";a:4:{s:4:"file";s:48:"hasin-farhan-7J-4ffEyF0Y-unsplash1-1980x1321.jpg";s:5:"width";i:1980;s:6:"height";i:1321;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}s:14:"original_image";s:38:"hasin-farhan-7J-4ffEyF0Y-unsplash1.jpg";}'),
(31, 29, '_wp_page_template', 'templates/template-full-width.php'),
(32, 35, '_edit_last', '1'),
(33, 35, '_edit_lock', '1590592327:1'),
(34, 39, '_wp_attached_file', '2020/05/dummy.pdf'),
(35, 39, '_wp_attachment_metadata', 'a:1:{s:5:"sizes";a:4:{s:4:"full";a:4:{s:4:"file";s:13:"dummy-pdf.jpg";s:5:"width";i:1058;s:6:"height";i:1497;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:21:"dummy-pdf-212x300.jpg";s:5:"width";i:212;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:22:"dummy-pdf-724x1024.jpg";s:5:"width";i:724;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:21:"dummy-pdf-106x150.jpg";s:5:"width";i:106;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}}'),
(36, 38, '_edit_last', '1'),
(37, 38, 'pdf_upload', '39'),
(38, 38, '_pdf_upload', 'field_5eb180147dd75'),
(39, 38, 'report_url', ''),
(40, 38, '_report_url', 'field_5eb1807a7dd76'),
(41, 38, '_edit_lock', '1588696955:1'),
(42, 38, 'report_label', 'May 2020'),
(43, 38, '_report_label', 'field_5eb181bab2dbe'),
(44, 42, '_edit_last', '1'),
(45, 42, '_edit_lock', '1590592423:1'),
(46, 42, 'pdf_upload', '39'),
(47, 42, '_pdf_upload', 'field_5eb180147dd75'),
(48, 42, 'report_url', ''),
(49, 42, '_report_url', 'field_5eb1807a7dd76'),
(50, 42, 'report_label', 'June 2020'),
(51, 42, '_report_label', 'field_5eb181bab2dbe'),
(52, 5, '_edit_lock', '1590592457:1'),
(53, 2, '_edit_last', '1'),
(54, 43, '_form', '<label> Your Name (required)\n    [text* your-name] </label>\n\n<label> Your Email (required)\n    [email* your-email] </label>\n\n<label> Subject\n    [text your-subject] </label>\n\n<label> Your Message\n    [textarea your-message] </label>\n\n[submit "Send"]'),
(55, 43, '_mail', 'a:8:{s:7:"subject";s:27:"wp-testing "[your-subject]"";s:6:"sender";s:41:"wp-testing <wordpress@wp-testing.docksal>";s:4:"body";s:182:"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on wp-testing (http://wp-testing.eclipse-dev.com)";s:9:"recipient";s:30:"kyle@eclipsemediasolutions.com";s:18:"additional_headers";s:22:"Reply-To: [your-email]";s:11:"attachments";s:0:"";s:8:"use_html";i:0;s:13:"exclude_blank";i:0;}'),
(56, 43, '_mail_2', 'a:9:{s:6:"active";b:0;s:7:"subject";s:27:"wp-testing "[your-subject]"";s:6:"sender";s:41:"wp-testing <wordpress@wp-testing.docksal>";s:4:"body";s:124:"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on wp-testing (http://wp-testing.eclipse-dev.com)";s:9:"recipient";s:12:"[your-email]";s:18:"additional_headers";s:40:"Reply-To: kyle@eclipsemediasolutions.com";s:11:"attachments";s:0:"";s:8:"use_html";i:0;s:13:"exclude_blank";i:0;}'),
(57, 43, '_messages', 'a:8:{s:12:"mail_sent_ok";s:45:"Thank you for your message. It has been sent.";s:12:"mail_sent_ng";s:71:"There was an error trying to send your message. Please try again later.";s:16:"validation_error";s:61:"One or more fields have an error. Please check and try again.";s:4:"spam";s:71:"There was an error trying to send your message. Please try again later.";s:12:"accept_terms";s:69:"You must accept the terms and conditions before sending your message.";s:16:"invalid_required";s:22:"The field is required.";s:16:"invalid_too_long";s:22:"The field is too long.";s:17:"invalid_too_short";s:23:"The field is too short.";}'),
(58, 43, '_additional_settings', NULL),
(59, 43, '_locale', 'en_US'),
(60, 44, '_wp_attached_file', '2020/05/lemon_sq_700w.jpg'),
(61, 44, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:700;s:6:"height";i:700;s:4:"file";s:25:"2020/05/lemon_sq_700w.jpg";s:5:"sizes";a:2:{s:6:"medium";a:4:{s:4:"file";s:25:"lemon_sq_700w-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:25:"lemon_sq_700w-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(62, 46, '_edit_last', '1'),
(63, 46, '_edit_lock', '1590607950:1'),
(64, 48, '_wp_attached_file', '2020/05/almonds_sq_700w.jpg'),
(65, 48, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:700;s:6:"height";i:700;s:4:"file";s:27:"2020/05/almonds_sq_700w.jpg";s:5:"sizes";a:2:{s:6:"medium";a:4:{s:4:"file";s:27:"almonds_sq_700w-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:27:"almonds_sq_700w-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(66, 50, '_wp_attached_file', '2020/05/almonds_sq_700w-1.jpg'),
(67, 50, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:700;s:6:"height";i:700;s:4:"file";s:29:"2020/05/almonds_sq_700w-1.jpg";s:5:"sizes";a:2:{s:6:"medium";a:4:{s:4:"file";s:29:"almonds_sq_700w-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:29:"almonds_sq_700w-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}') ;

#
# End of data contents of table `wp_postmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_posts`
#

DROP TABLE IF EXISTS `wp_posts`;


#
# Table structure of table `wp_posts`
#

CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_posts`
#
INSERT INTO `wp_posts` ( `ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2020-04-30 18:44:11', '2020-04-30 18:44:11', '<!-- wp:block {"ref":13} /-->\n\n<!-- wp:group {"align":"full"} -->\n<div class="wp-block-group alignfull"><div class="wp-block-group__inner-container"><!-- wp:heading {"align":"center"} -->\n<h2 class="has-text-align-center">is this a group heading?</h2>\n<!-- /wp:heading -->\n\n<!-- wp:group {"align":"full"} -->\n<div class="wp-block-group alignfull"><div class="wp-block-group__inner-container"><!-- wp:block-lab/card {"title":"card one","is-background-image":true,"image":8,"sub-text":"test","new-field":"wete"} /--></div></div>\n<!-- /wp:group --></div></div>\n<!-- /wp:group -->\n\n<!-- wp:image {"id":22,"sizeSlug":"full"} -->\n<figure class="wp-block-image size-full"><img src="http://wp-testing.eclipse-dev.com/wp-content/uploads/2020/04/colors.jpg" alt="" class="wp-image-22"/><figcaption>caption for this image<br></figcaption></figure>\n<!-- /wp:image -->\n\n<!-- wp:group -->\n<div class="wp-block-group"><div class="wp-block-group__inner-container"></div></div>\n<!-- /wp:group -->\n\n<!-- wp:columns -->\n<div class="wp-block-columns"><!-- wp:column -->\n<div class="wp-block-column"><!-- wp:block-lab/card {"title":"card one","is-background-image":true,"image":17,"sub-text":"wefwe","new-field":"wefew"} /--></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><!-- wp:block-lab/card {"title":"wetwefwe","image":8,"sub-text":"wefwef","new-field":"wefwe"} /--></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><!-- wp:block-lab/card {"title":"card title","is-background-image":true,"image":22,"sub-text":"tesv","new-field":"wefwe"} /--></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Hello world!', '', 'publish', 'closed', 'closed', '', 'hello-world', '', '', '2020-04-30 23:01:15', '2020-04-30 23:01:15', '', 0, 'http://wp-testing.eclipse-dev.com/?p=1', 0, 'post', '', 1),
(2, 1, '2020-04-30 18:44:11', '2020-04-30 18:44:11', '<!-- wp:block-lab/hero {"image":22,"headline":"Sample Page Hero","subtext":"Toil and Trouble","link":"https://vox.com","link-text":"vox"} /-->\n\n<!-- wp:paragraph {"dropCap":true} -->\n<p class="has-drop-cap">This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class="wp-block-quote"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like piña coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class="wp-block-quote"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href="http://wp-testing.eclipse-dev.com/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:block-lab/card {"title":"About","is-background-image":true,"image":44,"sub-text":"Learn about us"} /-->\n\n<!-- wp:paragraph -->\n<p>I\'m writing things. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:columns -->\n<div class="wp-block-columns"><!-- wp:column {"width":33.33} -->\n<div class="wp-block-column" style="flex-basis:33.33%"><!-- wp:block-lab/text-on-image {"text":"Almond Time","link":"https://waltwhitman.com","image":50} /--></div>\n<!-- /wp:column -->\n\n<!-- wp:column {"width":66.66} -->\n<div class="wp-block-column" style="flex-basis:66.66%"><!-- wp:block-lab/text-on-image {"text":"Big Time Lemons","link":"https://wp-testing.eclipse-dev.com/about","image":44} /--></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2020-05-27 19:52:01', '2020-05-27 19:52:01', '', 0, 'http://wp-testing.eclipse-dev.com/?page_id=2', 0, 'page', '', 0),
(3, 1, '2020-04-30 18:44:11', '2020-04-30 18:44:11', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://wp-testing.eclipse-dev.com.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {"level":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {"level":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {"level":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {"level":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {"level":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {"level":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {"level":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {"level":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {"level":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {"level":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {"level":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2020-04-30 18:44:11', '2020-04-30 18:44:11', '', 0, 'http://wp-testing.eclipse-dev.com/?page_id=3', 0, 'page', '', 0),
(5, 1, '2020-04-30 18:51:50', '0000-00-00 00:00:00', '{"block-lab\\/example-block":{"name":"example-block","title":"Example Block","icon":"block_lab","category":{"slug":"common","title":"Common Blocks","icon":null},"keywords":["sample","tutorial","template"],"fields":{"title":{"name":"title","label":"Title","control":"text","type":"string","location":"editor","order":0,"help":"The primary display text","default":"","placeholder":"","maxlength":null},"description":{"name":"description","label":"Description","control":"textarea","type":"string","location":"editor","order":1,"help":"","default":"","placeholder":"","maxlength":null,"number_rows":4},"button-text":{"name":"button-text","label":"Button Text","control":"text","type":"string","location":"editor","order":2,"help":"A Call-to-Action","default":"","placeholder":"","maxlength":null},"button-link":{"name":"button-link","label":"Button Link","control":"url","type":"string","location":"editor","order":3,"help":"The destination URL","default":"","placeholder":""}}}}', 'Example Block', '', 'draft', 'closed', 'closed', '', 'example-block', '', '', '2020-04-30 18:51:50', '0000-00-00 00:00:00', '', 0, 'http://wp-testing.eclipse-dev.com/?p=5', 0, 'block_lab', '', 0),
(6, 1, '2020-04-30 18:54:58', '2020-04-30 18:54:58', '{"block-lab\\/hero":{"name":"hero","title":"Hero","excluded":[],"icon":"credit_card","category":{"slug":"common","title":"Common Blocks","icon":null},"keywords":[""],"fields":{"image":{"name":"image","label":"image","control":"image","type":"integer","order":0,"location":"editor","width":"100","help":""},"headline":{"name":"headline","label":"headline","control":"text","type":"string","order":1,"location":"editor","width":"100","help":"","default":"","placeholder":"","maxlength":null},"subtext":{"name":"subtext","label":"subtext","control":"text","type":"string","order":2,"location":"editor","width":"100","help":"","default":"","placeholder":"","maxlength":null},"link":{"name":"link","label":"link","control":"text","type":"string","order":3,"location":"editor","width":"100","help":"","default":"","placeholder":"","maxlength":null},"link-text":{"name":"link-text","label":"link text","control":"text","type":"string","order":4,"location":"editor","width":"100","help":"","default":"","placeholder":"","maxlength":null}}}}', 'Hero', '', 'publish', 'closed', 'closed', '', 'hero', '', '', '2020-04-30 18:54:58', '2020-04-30 18:54:58', '', 0, 'http://wp-testing.eclipse-dev.com/?post_type=block_lab&#038;p=6', 0, 'block_lab', '', 0),
(8, 1, '2020-04-30 20:21:13', '2020-04-30 20:21:13', '', 'lilac', '', 'inherit', 'open', 'closed', '', 'lilac', '', '', '2020-04-30 20:21:13', '2020-04-30 20:21:13', '', 1, 'http://wp-testing.eclipse-dev.com/wp-content/uploads/2020/04/lilac.jpg', 0, 'attachment', 'image/jpeg', 0),
(9, 1, '2020-04-30 20:21:56', '2020-04-30 20:21:56', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:block-lab/hero {"image":8,"headline":"We\'ve got the stuff you want.","subtext":"So come get it.","link":"theatlantic.com","link-text":"get it now"} /-->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2020-04-30 20:21:56', '2020-04-30 20:21:56', '', 1, 'http://wp-testing.eclipse-dev.com/2020/04/30/1-revision-v1/', 0, 'revision', '', 0),
(10, 1, '2020-04-30 20:39:28', '2020-04-30 20:39:28', '<!-- wp:block-lab/hero {"image":8,"headline":"We\'ve got the stuff you want.","subtext":"So come get it.","link":"theatlantic.com","link-text":"get it now"} /-->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2020-04-30 20:39:28', '2020-04-30 20:39:28', '', 1, 'http://wp-testing.eclipse-dev.com/2020/04/30/1-revision-v1/', 0, 'revision', '', 0),
(12, 1, '2020-04-30 20:53:35', '2020-04-30 20:53:35', '<!-- wp:block-lab/hero {"image":8,"headline":"We\'ve got the stuff you want.","subtext":"So come get it.","link":"https://theatlantic.com","link-text":"get it now"} /-->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2020-04-30 20:53:35', '2020-04-30 20:53:35', '', 1, 'http://wp-testing.eclipse-dev.com/2020/04/30/1-revision-v1/', 0, 'revision', '', 0),
(13, 1, '2020-04-30 20:53:55', '2020-04-30 20:53:55', '<!-- wp:block-lab/hero {"image":8,"headline":"We\'ve got the stuff you want.","subtext":"So come get it.","link":"https://theatlantic.com","link-text":"get it now"} /-->', 'Hero reusable', '', 'publish', 'closed', 'closed', '', 'untitled-reusable-block', '', '', '2020-04-30 20:54:02', '2020-04-30 20:54:02', '', 0, 'http://wp-testing.eclipse-dev.com/2020/04/30/untitled-reusable-block/', 0, 'wp_block', '', 0),
(14, 1, '2020-04-30 20:54:29', '2020-04-30 20:54:29', '<!-- wp:block {"ref":13} /-->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2020-04-30 20:54:29', '2020-04-30 20:54:29', '', 1, 'http://wp-testing.eclipse-dev.com/2020/04/30/1-revision-v1/', 0, 'revision', '', 0),
(16, 1, '2020-04-30 22:16:51', '2020-04-30 22:16:51', '{"block-lab\\/card":{"name":"card","title":"Card","excluded":[],"icon":"block_lab","category":{"slug":"reusable","title":"Reusable Blocks","icon":null},"keywords":["card"],"fields":{"title":{"name":"title","label":"Title","control":"text","type":"string","order":0,"location":"editor","width":"100","help":"","default":"","placeholder":"","maxlength":null},"is-background-image":{"name":"is-background-image","label":"Is Background image","control":"checkbox","type":"boolean","order":1,"location":"editor","width":"100","help":"Check this box if the image should be a background image on the card. Unchecked will display the image on the bottom half of the card.","default":0},"image":{"name":"image","label":"Image","control":"image","type":"integer","order":2,"location":"editor","width":"100","help":""},"sub-text":{"name":"sub-text","label":"Sub Text","control":"textarea","type":"textarea","order":3,"location":"editor","width":"100","help":"","default":"","placeholder":"","maxlength":null,"number_rows":4,"new_lines":"autop"}}}}', 'Card', '', 'publish', 'closed', 'closed', '', 'card', '', '', '2020-05-27 19:06:25', '2020-05-27 19:06:25', '', 0, 'http://wp-testing.eclipse-dev.com/?post_type=block_lab&#038;p=16', 0, 'block_lab', '', 0),
(17, 1, '2020-04-30 22:27:42', '2020-04-30 22:27:42', '', 'bicycle', '', 'inherit', 'open', 'closed', '', 'bicycle', '', '', '2020-04-30 22:27:42', '2020-04-30 22:27:42', '', 1, 'http://wp-testing.eclipse-dev.com/wp-content/uploads/2020/04/bicycle.jpg', 0, 'attachment', 'image/jpeg', 0),
(19, 1, '2020-04-30 22:28:08', '2020-04-30 22:28:08', '<!-- wp:block {"ref":13} /-->\n\n<!-- wp:block-lab/card {"title":"Static","is-background-image":true,"image":17,"sub-text":"but don\'t tell him that.","new-field":"whta is this field?"} /-->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2020-04-30 22:28:08', '2020-04-30 22:28:08', '', 1, 'http://wp-testing.eclipse-dev.com/2020/04/30/1-revision-v1/', 0, 'revision', '', 0),
(20, 1, '2020-04-30 22:42:15', '2020-04-30 22:42:15', '<!-- wp:block {"ref":13} /-->\n\n<!-- wp:block-lab/card {"title":"Static","is-background-image":true,"image":17,"sub-text":"but don\'t tell him that.","new-field":"whta is this field?"} /-->\n\n<!-- wp:group -->\n<div class="wp-block-group"><div class="wp-block-group__inner-container"><!-- wp:block-lab/card {"title":"card one","is-background-image":true,"image":8,"sub-text":"test","new-field":"wete"} /--></div></div>\n<!-- /wp:group -->\n\n<!-- wp:block-lab/card {"title":"test","is-background-image":false,"sub-text":"stewe","new-field":"wefwe"} /-->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2020-04-30 22:42:15', '2020-04-30 22:42:15', '', 1, 'http://wp-testing.eclipse-dev.com/2020/04/30/1-revision-v1/', 0, 'revision', '', 0),
(21, 1, '2020-04-30 22:43:13', '2020-04-30 22:43:13', '<!-- wp:block {"ref":13} /-->\n\n<!-- wp:block-lab/card {"title":"Static","is-background-image":true,"image":17,"sub-text":"but don\'t tell him that.","new-field":"whta is this field?"} /-->\n\n<!-- wp:group -->\n<div class="wp-block-group"><div class="wp-block-group__inner-container"><!-- wp:block-lab/card {"title":"card one","is-background-image":true,"image":8,"sub-text":"test","new-field":"wete"} /--></div></div>\n<!-- /wp:group -->\n\n<!-- wp:block-lab/card {"title":"will this card be inthe group","image":17,"sub-text":"wetwe","new-field":"wefwe"} /-->\n\n<!-- wp:block-lab/card {"title":"test","is-background-image":false,"sub-text":"stewe","new-field":"wefwe"} /-->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2020-04-30 22:43:13', '2020-04-30 22:43:13', '', 1, 'http://wp-testing.eclipse-dev.com/2020/04/30/1-revision-v1/', 0, 'revision', '', 0),
(22, 1, '2020-04-30 22:44:20', '2020-04-30 22:44:20', '', 'colors', '', 'inherit', 'open', 'closed', '', 'colors', '', '', '2020-04-30 22:44:20', '2020-04-30 22:44:20', '', 1, 'http://wp-testing.eclipse-dev.com/wp-content/uploads/2020/04/colors.jpg', 0, 'attachment', 'image/jpeg', 0),
(23, 1, '2020-04-30 22:44:27', '2020-04-30 22:44:27', '<!-- wp:block {"ref":13} /-->\n\n<!-- wp:block-lab/card {"title":"Static","is-background-image":true,"image":17,"sub-text":"but don\'t tell him that.","new-field":"whta is this field?"} /-->\n\n<!-- wp:group -->\n<div class="wp-block-group"><div class="wp-block-group__inner-container"><!-- wp:block-lab/card {"title":"card one","is-background-image":true,"image":8,"sub-text":"test","new-field":"wete"} /--></div></div>\n<!-- /wp:group -->\n\n<!-- wp:block-lab/card {"title":"will this card be inthe group","image":17,"sub-text":"wetwe","new-field":"wefwe"} /-->\n\n<!-- wp:block-lab/card {"title":"test","is-background-image":false,"sub-text":"stewe","new-field":"wefwe"} /-->\n\n<!-- wp:columns -->\n<div class="wp-block-columns"><!-- wp:column -->\n<div class="wp-block-column"><!-- wp:block-lab/card {"title":"card one","is-background-image":true,"image":17,"sub-text":"wefwe","new-field":"wefew"} /--></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><!-- wp:block-lab/card {"title":"wetwefwe","image":8,"sub-text":"wefwef","new-field":"wefwe"} /--></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><!-- wp:block-lab/card {"title":"card title","is-background-image":true,"image":22,"sub-text":"tesv","new-field":"wefwe"} /--></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2020-04-30 22:44:27', '2020-04-30 22:44:27', '', 1, 'http://wp-testing.eclipse-dev.com/2020/04/30/1-revision-v1/', 0, 'revision', '', 0),
(24, 1, '2020-04-30 22:50:09', '2020-04-30 22:50:09', '<!-- wp:block {"ref":13} /-->\n\n<!-- wp:block-lab/card {"title":"Static","is-background-image":true,"image":17,"sub-text":"but don\'t tell him that.","new-field":"whta is this field?"} /-->\n\n<!-- wp:group -->\n<div class="wp-block-group"><div class="wp-block-group__inner-container"><!-- wp:block-lab/card {"title":"card one","is-background-image":true,"image":8,"sub-text":"test","new-field":"wete"} /--></div></div>\n<!-- /wp:group -->\n\n<!-- wp:heading -->\n<h2>is this a group heading?</h2>\n<!-- /wp:heading -->\n\n<!-- wp:block-lab/card {"title":"will this card be inthe group","image":17,"sub-text":"wetwe","new-field":"wefwe"} /-->\n\n<!-- wp:block-lab/card {"title":"test","is-background-image":false,"sub-text":"stewe","new-field":"wefwe"} /-->\n\n<!-- wp:columns -->\n<div class="wp-block-columns"><!-- wp:column -->\n<div class="wp-block-column"><!-- wp:block-lab/card {"title":"card one","is-background-image":true,"image":17,"sub-text":"wefwe","new-field":"wefew"} /--></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><!-- wp:block-lab/card {"title":"wetwefwe","image":8,"sub-text":"wefwef","new-field":"wefwe"} /--></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><!-- wp:block-lab/card {"title":"card title","is-background-image":true,"image":22,"sub-text":"tesv","new-field":"wefwe"} /--></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2020-04-30 22:50:09', '2020-04-30 22:50:09', '', 1, 'http://wp-testing.eclipse-dev.com/2020/04/30/1-revision-v1/', 0, 'revision', '', 0),
(26, 1, '2020-04-30 23:00:17', '2020-04-30 23:00:17', '<!-- wp:block {"ref":13} /-->\n\n<!-- wp:block-lab/card {"title":"Static","is-background-image":true,"image":17,"sub-text":"but don\'t tell him that.","new-field":"whta is this field?"} /-->\n\n<!-- wp:group {"align":"full"} -->\n<div class="wp-block-group alignfull"><div class="wp-block-group__inner-container"><!-- wp:heading {"align":"center"} -->\n<h2 class="has-text-align-center">is this a group heading?</h2>\n<!-- /wp:heading -->\n\n<!-- wp:group {"align":"full"} -->\n<div class="wp-block-group alignfull"><div class="wp-block-group__inner-container"><!-- wp:block-lab/card {"title":"card one","is-background-image":true,"image":8,"sub-text":"test","new-field":"wete"} /--></div></div>\n<!-- /wp:group --></div></div>\n<!-- /wp:group -->\n\n<!-- wp:image {"id":22,"sizeSlug":"full"} -->\n<figure class="wp-block-image size-full"><img src="http://wp-testing.eclipse-dev.com/wp-content/uploads/2020/04/colors.jpg" alt="" class="wp-image-22"/><figcaption>caption for this image<br></figcaption></figure>\n<!-- /wp:image -->\n\n<!-- wp:group -->\n<div class="wp-block-group"><div class="wp-block-group__inner-container"><!-- wp:block-lab/card {"title":"will this card be inthe group","image":17,"sub-text":"wetwe","new-field":"wefwe"} /--></div></div>\n<!-- /wp:group -->\n\n<!-- wp:block-lab/card {"title":"test","is-background-image":false,"sub-text":"stewe","new-field":"wefwe"} /-->\n\n<!-- wp:columns -->\n<div class="wp-block-columns"><!-- wp:column -->\n<div class="wp-block-column"><!-- wp:block-lab/card {"title":"card one","is-background-image":true,"image":17,"sub-text":"wefwe","new-field":"wefew"} /--></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><!-- wp:block-lab/card {"title":"wetwefwe","image":8,"sub-text":"wefwef","new-field":"wefwe"} /--></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><!-- wp:block-lab/card {"title":"card title","is-background-image":true,"image":22,"sub-text":"tesv","new-field":"wefwe"} /--></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2020-04-30 23:00:17', '2020-04-30 23:00:17', '', 1, 'http://wp-testing.eclipse-dev.com/2020/04/30/1-revision-v1/', 0, 'revision', '', 0),
(27, 1, '2020-04-30 23:01:15', '2020-04-30 23:01:15', '<!-- wp:block {"ref":13} /-->\n\n<!-- wp:group {"align":"full"} -->\n<div class="wp-block-group alignfull"><div class="wp-block-group__inner-container"><!-- wp:heading {"align":"center"} -->\n<h2 class="has-text-align-center">is this a group heading?</h2>\n<!-- /wp:heading -->\n\n<!-- wp:group {"align":"full"} -->\n<div class="wp-block-group alignfull"><div class="wp-block-group__inner-container"><!-- wp:block-lab/card {"title":"card one","is-background-image":true,"image":8,"sub-text":"test","new-field":"wete"} /--></div></div>\n<!-- /wp:group --></div></div>\n<!-- /wp:group -->\n\n<!-- wp:image {"id":22,"sizeSlug":"full"} -->\n<figure class="wp-block-image size-full"><img src="http://wp-testing.eclipse-dev.com/wp-content/uploads/2020/04/colors.jpg" alt="" class="wp-image-22"/><figcaption>caption for this image<br></figcaption></figure>\n<!-- /wp:image -->\n\n<!-- wp:group -->\n<div class="wp-block-group"><div class="wp-block-group__inner-container"></div></div>\n<!-- /wp:group -->\n\n<!-- wp:columns -->\n<div class="wp-block-columns"><!-- wp:column -->\n<div class="wp-block-column"><!-- wp:block-lab/card {"title":"card one","is-background-image":true,"image":17,"sub-text":"wefwe","new-field":"wefew"} /--></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><!-- wp:block-lab/card {"title":"wetwefwe","image":8,"sub-text":"wefwef","new-field":"wefwe"} /--></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><!-- wp:block-lab/card {"title":"card title","is-background-image":true,"image":22,"sub-text":"tesv","new-field":"wefwe"} /--></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2020-04-30 23:01:15', '2020-04-30 23:01:15', '', 1, 'http://wp-testing.eclipse-dev.com/2020/04/30/1-revision-v1/', 0, 'revision', '', 0),
(28, 1, '2020-04-30 23:06:33', '2020-04-30 23:06:33', '<!-- wp:block-lab/hero {"image":22,"headline":"Sample Page Hero","subtext":"Toil and Trouble","link":"https://vox.com","link-text":"vox"} /-->\n\n<!-- wp:paragraph {"dropCap":true} -->\n<p class="has-drop-cap">This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class="wp-block-quote"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like piña coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class="wp-block-quote"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href="http://wp-testing.eclipse-dev.com/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2020-04-30 23:06:33', '2020-04-30 23:06:33', '', 2, 'http://wp-testing.eclipse-dev.com/2020/04/30/2-revision-v1/', 0, 'revision', '', 0),
(29, 1, '2020-04-30 23:17:11', '2020-04-30 23:17:11', '<!-- wp:paragraph -->\n<p>There\'s an oak tree outside of my window. The tree is old and tall. It\'s leaves are growing bigger every day because it\'s spring. Eventually it will be fall and the leaves will shrivel and die. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class="wp-block-quote"><p>Those who tell the truth shall die. Those who tell the truth shall live forever.</p><cite>Explosions in the Sky<br></cite></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:heading -->\n<h2>Nonetheless</h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>We can\'t stare at the tree all day though. The tasks of the day require our attention and so we set out to do them. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:block-lab/hero {"image":32} /-->', 'Oak Tree', '', 'publish', 'open', 'open', '', 'oak-tree', '', '', '2020-04-30 23:24:42', '2020-04-30 23:24:42', '', 0, 'http://wp-testing.eclipse-dev.com/?p=29', 0, 'post', '', 0),
(30, 1, '2020-04-30 23:17:11', '2020-04-30 23:17:11', '<!-- wp:paragraph -->\n<p>There\'s an oak tree outside of my window. The tree is old and tall. It\'s leaves are growing bigger every day because it\'s spring. Eventually it will be fall and the leaves will shrivel and die. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class="wp-block-quote"><p>Those who tell the truth shall die. Those who tell the truth shall live forever.</p><cite>Explosions in the Sky<br></cite></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:heading -->\n<h2>Nonetheless</h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>We can\'t stare at the tree all day though. The tasks of the day require our attention and so we set out to do them. </p>\n<!-- /wp:paragraph -->', 'Oak Tree', '', 'inherit', 'closed', 'closed', '', '29-revision-v1', '', '', '2020-04-30 23:17:11', '2020-04-30 23:17:11', '', 29, 'http://wp-testing.eclipse-dev.com/2020/04/30/29-revision-v1/', 0, 'revision', '', 0),
(32, 1, '2020-04-30 23:22:06', '2020-04-30 23:22:06', '', 'hasin-farhan-7J-4ffEyF0Y-unsplash1', '', 'inherit', 'open', 'closed', '', 'hasin-farhan-7j-4ffeyf0y-unsplash1', '', '', '2020-04-30 23:22:06', '2020-04-30 23:22:06', '', 29, 'http://wp-testing.eclipse-dev.com/wp-content/uploads/2020/04/hasin-farhan-7J-4ffEyF0Y-unsplash1.jpg', 0, 'attachment', 'image/jpeg', 0),
(33, 1, '2020-04-30 23:22:13', '2020-04-30 23:22:13', '<!-- wp:paragraph -->\n<p>There\'s an oak tree outside of my window. The tree is old and tall. It\'s leaves are growing bigger every day because it\'s spring. Eventually it will be fall and the leaves will shrivel and die. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class="wp-block-quote"><p>Those who tell the truth shall die. Those who tell the truth shall live forever.</p><cite>Explosions in the Sky<br></cite></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:heading -->\n<h2>Nonetheless</h2>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>We can\'t stare at the tree all day though. The tasks of the day require our attention and so we set out to do them. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:block-lab/hero {"image":32} /-->', 'Oak Tree', '', 'inherit', 'closed', 'closed', '', '29-revision-v1', '', '', '2020-04-30 23:22:13', '2020-04-30 23:22:13', '', 29, 'http://wp-testing.eclipse-dev.com/2020/04/30/29-revision-v1/', 0, 'revision', '', 0),
(35, 1, '2020-05-05 15:05:05', '2020-05-05 15:05:05', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:6:"report";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'Reports', 'reports', 'publish', 'closed', 'closed', '', 'group_5eb17ffce544d', '', '', '2020-05-05 15:10:04', '2020-05-05 15:10:04', '', 0, 'http://wp-testing.eclipse-dev.com/?post_type=acf-field-group&#038;p=35', 0, 'acf-field-group', '', 0),
(36, 1, '2020-05-05 15:05:05', '2020-05-05 15:05:05', 'a:10:{s:4:"type";s:4:"file";s:12:"instructions";s:18:"Upload report PDFs";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:18:"report-pdf-wrapper";s:2:"id";s:0:"";}s:13:"return_format";s:3:"url";s:7:"library";s:3:"all";s:8:"min_size";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'PDF upload', 'pdf_upload', 'publish', 'closed', 'closed', '', 'field_5eb180147dd75', '', '', '2020-05-05 15:05:05', '2020-05-05 15:05:05', '', 35, 'http://wp-testing.eclipse-dev.com/?post_type=acf-field&p=36', 0, 'acf-field', '', 0),
(37, 1, '2020-05-05 15:05:05', '2020-05-05 15:05:05', 'a:7:{s:4:"type";s:3:"url";s:12:"instructions";s:19:"Paste in report URL";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:18:"report-url-wrapper";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";}', 'Report URL', 'report_url', 'publish', 'closed', 'closed', '', 'field_5eb1807a7dd76', '', '', '2020-05-05 15:05:05', '2020-05-05 15:05:05', '', 35, 'http://wp-testing.eclipse-dev.com/?post_type=acf-field&p=37', 1, 'acf-field', '', 0),
(38, 1, '2020-05-05 15:09:37', '2020-05-05 15:09:37', '', 'May 2020', '', 'publish', 'open', 'closed', '', '38', '', '', '2020-05-05 15:10:29', '2020-05-05 15:10:29', '', 0, 'http://wp-testing.eclipse-dev.com/?post_type=report&#038;p=38', 0, 'report', '', 0),
(39, 1, '2020-05-05 15:09:26', '2020-05-05 15:09:26', '', 'dummy', '', 'inherit', 'open', 'closed', '', 'dummy', '', '', '2020-05-05 15:09:26', '2020-05-05 15:09:26', '', 38, 'http://wp-testing.eclipse-dev.com/wp-content/uploads/2020/05/dummy.pdf', 0, 'attachment', 'application/pdf', 0),
(40, 1, '2020-05-05 15:10:04', '2020-05-05 15:10:04', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Report Label', 'report_label', 'publish', 'closed', 'closed', '', 'field_5eb181bab2dbe', '', '', '2020-05-05 15:10:04', '2020-05-05 15:10:04', '', 35, 'http://wp-testing.eclipse-dev.com/?post_type=acf-field&p=40', 2, 'acf-field', '', 0),
(41, 1, '2020-05-27 15:11:02', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2020-05-27 15:11:02', '0000-00-00 00:00:00', '', 0, 'http://wp-testing.eclipse-dev.com/?p=41', 0, 'post', '', 0),
(42, 1, '2020-05-27 15:15:06', '2020-05-27 15:15:06', 'This was a big report.', 'June 2020', '', 'publish', 'open', 'closed', '', 'june-2020', '', '', '2020-05-27 15:15:06', '2020-05-27 15:15:06', '', 0, 'http://wp-testing.eclipse-dev.com/?post_type=report&#038;p=42', 0, 'report', '', 0),
(43, 1, '2020-05-27 19:03:48', '2020-05-27 19:03:48', '<label> Your Name (required)\n    [text* your-name] </label>\n\n<label> Your Email (required)\n    [email* your-email] </label>\n\n<label> Subject\n    [text your-subject] </label>\n\n<label> Your Message\n    [textarea your-message] </label>\n\n[submit "Send"]\nwp-testing "[your-subject]"\nwp-testing <wordpress@wp-testing.docksal>\nFrom: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on wp-testing (http://wp-testing.eclipse-dev.com)\nkyle@eclipsemediasolutions.com\nReply-To: [your-email]\n\n0\n0\n\nwp-testing "[your-subject]"\nwp-testing <wordpress@wp-testing.docksal>\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on wp-testing (http://wp-testing.eclipse-dev.com)\n[your-email]\nReply-To: kyle@eclipsemediasolutions.com\n\n0\n0\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.', 'Contact form 1', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2020-05-27 19:03:48', '2020-05-27 19:03:48', '', 0, 'http://wp-testing.eclipse-dev.com/?post_type=wpcf7_contact_form&p=43', 0, 'wpcf7_contact_form', '', 0),
(44, 1, '2020-05-27 19:05:17', '2020-05-27 19:05:17', '', 'lemon_sq_700w', '', 'inherit', 'open', 'closed', '', 'lemon_sq_700w', '', '', '2020-05-27 19:05:17', '2020-05-27 19:05:17', '', 2, 'http://wp-testing.eclipse-dev.com/wp-content/uploads/2020/05/lemon_sq_700w.jpg', 0, 'attachment', 'image/jpeg', 0),
(45, 1, '2020-05-27 19:05:38', '2020-05-27 19:05:38', '<!-- wp:block-lab/hero {"image":22,"headline":"Sample Page Hero","subtext":"Toil and Trouble","link":"https://vox.com","link-text":"vox"} /-->\n\n<!-- wp:paragraph {"dropCap":true} -->\n<p class="has-drop-cap">This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class="wp-block-quote"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like piña coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class="wp-block-quote"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href="http://wp-testing.eclipse-dev.com/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:block-lab/card {"title":"About","is-background-image":true,"image":44,"sub-text":"Learn about us","new-field":"What is this new field?"} /-->', 'Sample Page', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2020-05-27 19:05:38', '2020-05-27 19:05:38', '', 2, 'http://wp-testing.eclipse-dev.com/2020/05/27/2-revision-v1/', 0, 'revision', '', 0),
(46, 1, '2020-05-27 19:08:24', '2020-05-27 19:08:24', '{"block-lab\\/text-on-image":{"name":"text-on-image","title":"Text on Image","excluded":[],"icon":"block_lab","category":{"slug":"common","title":"Common Blocks","icon":null},"keywords":[""],"fields":{"text":{"name":"text","label":"Text","control":"text","type":"string","order":0,"location":"editor","width":"100","help":"Text that shows over image","default":"","placeholder":"","maxlength":null},"link":{"name":"link","label":"Link","control":"url","type":"string","order":1,"location":"editor","width":"100","help":"","default":"","placeholder":""},"image":{"name":"image","label":"Image","control":"image","type":"integer","order":2,"location":"editor","width":"100","help":""}}}}', 'Text on Image', '', 'publish', 'closed', 'closed', '', 'text-on-image', '', '', '2020-05-27 19:08:24', '2020-05-27 19:08:24', '', 0, 'http://wp-testing.eclipse-dev.com/?post_type=block_lab&#038;p=46', 0, 'block_lab', '', 0),
(48, 1, '2020-05-27 19:25:30', '2020-05-27 19:25:30', '', 'almonds_sq_700w', '', 'inherit', 'open', 'closed', '', 'almonds_sq_700w', '', '', '2020-05-27 19:25:30', '2020-05-27 19:25:30', '', 2, 'http://wp-testing.eclipse-dev.com/wp-content/uploads/2020/05/almonds_sq_700w.jpg', 0, 'attachment', 'image/jpeg', 0),
(49, 1, '2020-05-27 19:25:33', '2020-05-27 19:25:33', '<!-- wp:block-lab/hero {"image":22,"headline":"Sample Page Hero","subtext":"Toil and Trouble","link":"https://vox.com","link-text":"vox"} /-->\n\n<!-- wp:paragraph {"dropCap":true} -->\n<p class="has-drop-cap">This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class="wp-block-quote"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like piña coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class="wp-block-quote"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href="http://wp-testing.eclipse-dev.com/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:block-lab/card {"title":"About","is-background-image":true,"image":44,"sub-text":"Learn about us"} /-->\n\n<!-- wp:block-lab/text-on-image {"text":"Walt Whitman","link":"https://waltwhitman.com","image":48} /-->', 'Sample Page', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2020-05-27 19:25:33', '2020-05-27 19:25:33', '', 2, 'http://wp-testing.eclipse-dev.com/2020/05/27/2-revision-v1/', 0, 'revision', '', 0),
(50, 1, '2020-05-27 19:31:22', '2020-05-27 19:31:22', '', 'almonds_sq_700w-1', '', 'inherit', 'open', 'closed', '', 'almonds_sq_700w-1', '', '', '2020-05-27 19:31:22', '2020-05-27 19:31:22', '', 2, 'http://wp-testing.eclipse-dev.com/wp-content/uploads/2020/05/almonds_sq_700w-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(51, 1, '2020-05-27 19:31:24', '2020-05-27 19:31:24', '<!-- wp:block-lab/hero {"image":22,"headline":"Sample Page Hero","subtext":"Toil and Trouble","link":"https://vox.com","link-text":"vox"} /-->\n\n<!-- wp:paragraph {"dropCap":true} -->\n<p class="has-drop-cap">This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class="wp-block-quote"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like piña coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class="wp-block-quote"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href="http://wp-testing.eclipse-dev.com/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:block-lab/card {"title":"About","is-background-image":true,"image":44,"sub-text":"Learn about us"} /-->\n\n<!-- wp:block-lab/text-on-image {"text":"Walt Whitman","link":"https://waltwhitman.com","image":48} /-->\n\n<!-- wp:block-lab/text-on-image {"text":"Whitman","link":"https://waltwhitman.com","image":50} /-->', 'Sample Page', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2020-05-27 19:31:24', '2020-05-27 19:31:24', '', 2, 'http://wp-testing.eclipse-dev.com/2020/05/27/2-revision-v1/', 0, 'revision', '', 0),
(52, 1, '2020-05-27 19:32:16', '2020-05-27 19:32:16', '<!-- wp:block-lab/hero {"image":22,"headline":"Sample Page Hero","subtext":"Toil and Trouble","link":"https://vox.com","link-text":"vox"} /-->\n\n<!-- wp:paragraph {"dropCap":true} -->\n<p class="has-drop-cap">This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class="wp-block-quote"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like piña coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class="wp-block-quote"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href="http://wp-testing.eclipse-dev.com/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:block-lab/card {"title":"About","is-background-image":true,"image":44,"sub-text":"Learn about us"} /-->\n\n<!-- wp:block-lab/text-on-image {"text":"Walt Whitman","link":"https://waltwhitman.com","image":48} /-->\n\n<!-- wp:block-lab/text-on-image {"text":"Whitman","link":"https://waltwhitman.com","image":50} /-->\n\n<!-- wp:paragraph -->\n<p>I\'m writing things. </p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2020-05-27 19:32:16', '2020-05-27 19:32:16', '', 2, 'http://wp-testing.eclipse-dev.com/2020/05/27/2-revision-v1/', 0, 'revision', '', 0),
(53, 1, '2020-05-27 19:32:58', '2020-05-27 19:32:58', '<!-- wp:block-lab/hero {"image":22,"headline":"Sample Page Hero","subtext":"Toil and Trouble","link":"https://vox.com","link-text":"vox"} /-->\n\n<!-- wp:paragraph {"dropCap":true} -->\n<p class="has-drop-cap">This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class="wp-block-quote"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like piña coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class="wp-block-quote"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href="http://wp-testing.eclipse-dev.com/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:block-lab/card {"title":"About","is-background-image":true,"image":44,"sub-text":"Learn about us"} /-->\n\n<!-- wp:block-lab/text-on-image {"text":"Walt Whitman","link":"https://waltwhitman.com","image":48} /-->\n\n<!-- wp:block-lab/text-on-image {"text":"Whitman","link":"https://waltwhitman.com","image":50} /-->\n\n<!-- wp:paragraph -->\n<p>I\'m writing things. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:block-lab/text-on-image {"text":"Whitman","link":"https://waltwhitman.com","image":50} /-->', 'Sample Page', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2020-05-27 19:32:58', '2020-05-27 19:32:58', '', 2, 'http://wp-testing.eclipse-dev.com/2020/05/27/2-revision-v1/', 0, 'revision', '', 0) ;
INSERT INTO `wp_posts` ( `ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(54, 1, '2020-05-27 19:52:01', '2020-05-27 19:52:01', '<!-- wp:block-lab/hero {"image":22,"headline":"Sample Page Hero","subtext":"Toil and Trouble","link":"https://vox.com","link-text":"vox"} /-->\n\n<!-- wp:paragraph {"dropCap":true} -->\n<p class="has-drop-cap">This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class="wp-block-quote"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like piña coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class="wp-block-quote"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href="http://wp-testing.eclipse-dev.com/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:block-lab/card {"title":"About","is-background-image":true,"image":44,"sub-text":"Learn about us"} /-->\n\n<!-- wp:paragraph -->\n<p>I\'m writing things. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:columns -->\n<div class="wp-block-columns"><!-- wp:column {"width":33.33} -->\n<div class="wp-block-column" style="flex-basis:33.33%"><!-- wp:block-lab/text-on-image {"text":"Almond Time","link":"https://waltwhitman.com","image":50} /--></div>\n<!-- /wp:column -->\n\n<!-- wp:column {"width":66.66} -->\n<div class="wp-block-column" style="flex-basis:66.66%"><!-- wp:block-lab/text-on-image {"text":"Big Time Lemons","link":"https://wp-testing.eclipse-dev.com/about","image":44} /--></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Sample Page', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2020-05-27 19:52:01', '2020-05-27 19:52:01', '', 2, 'http://wp-testing.eclipse-dev.com/2020/05/27/2-revision-v1/', 0, 'revision', '', 0) ;

#
# End of data contents of table `wp_posts`
# --------------------------------------------------------



#
# Delete any existing table `wp_term_relationships`
#

DROP TABLE IF EXISTS `wp_term_relationships`;


#
# Table structure of table `wp_term_relationships`
#

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_term_relationships`
#
INSERT INTO `wp_term_relationships` ( `object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(29, 1, 0) ;

#
# End of data contents of table `wp_term_relationships`
# --------------------------------------------------------



#
# Delete any existing table `wp_term_taxonomy`
#

DROP TABLE IF EXISTS `wp_term_taxonomy`;


#
# Table structure of table `wp_term_taxonomy`
#

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_term_taxonomy`
#
INSERT INTO `wp_term_taxonomy` ( `term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 2) ;

#
# End of data contents of table `wp_term_taxonomy`
# --------------------------------------------------------



#
# Delete any existing table `wp_termmeta`
#

DROP TABLE IF EXISTS `wp_termmeta`;


#
# Table structure of table `wp_termmeta`
#

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_termmeta`
#

#
# End of data contents of table `wp_termmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_terms`
#

DROP TABLE IF EXISTS `wp_terms`;


#
# Table structure of table `wp_terms`
#

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_terms`
#
INSERT INTO `wp_terms` ( `term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0) ;

#
# End of data contents of table `wp_terms`
# --------------------------------------------------------



#
# Delete any existing table `wp_usermeta`
#

DROP TABLE IF EXISTS `wp_usermeta`;


#
# Table structure of table `wp_usermeta`
#

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_usermeta`
#
INSERT INTO `wp_usermeta` ( `umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'theme_editor_notice'),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:2:{s:64:"0a149f9eea6e6c6c3e73255ec9aa6450e1660bbf11eb53613c5026d859aa7818";a:4:{s:10:"expiration";i:1590765062;s:2:"ip";s:10:"172.24.0.5";s:2:"ua";s:82:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:76.0) Gecko/20100101 Firefox/76.0";s:5:"login";i:1590592262;}s:64:"b5ba38d45bb4a14a513da01260f135e533187a291a89cc1f32e244a1f8e3b2d5";a:4:{s:10:"expiration";i:1590778938;s:2:"ip";s:10:"172.24.0.2";s:2:"ua";s:82:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:76.0) Gecko/20100101 Firefox/76.0";s:5:"login";i:1590606138;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '41'),
(18, 1, 'community-events-location', 'a:1:{s:2:"ip";s:12:"192.168.64.0";}'),
(19, 1, 'wp_user-settings', 'libraryContent=browse'),
(20, 1, 'wp_user-settings-time', '1588286537') ;

#
# End of data contents of table `wp_usermeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_users`
#

DROP TABLE IF EXISTS `wp_users`;


#
# Table structure of table `wp_users`
#

CREATE TABLE `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_users`
#
INSERT INTO `wp_users` ( `ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$BgeTIQOvFzRPtouolktazDDtnhhHAP1', 'admin', 'kyle@eclipsemediasolutions.com', 'http://wp-testing.eclipse-dev.com', '2020-04-30 18:44:11', '', 0, 'admin') ;

#
# End of data contents of table `wp_users`
# --------------------------------------------------------

#
# Add constraints back in and apply any alter data queries.
#


<?php
/**
 * The Template for displaying all single posts.
 *
 * @package GeneratePress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header(); 

$report_label = get_field('report_label');
$report_pdf = get_field('pdf_upload');
$report_url = get_field('report_url');

?>

	<div id="primary" <?php generate_do_element_classes( 'content' ); ?>>
		<main id="main" <?php generate_do_element_classes( 'main' ); ?>>
			<?php
			/**
			 * generate_before_main_content hook.
			 *
			 * @since 0.1
			 */
			do_action( 'generate_before_main_content' );

			if($report_label): ?>
			<div class="report-wrapper">
				<?php echo var_export($GLOBALS['post'], TRUE); ?>
				<?php echo var_export($report_pdf, TRUE); ?>
				<p>Download the report below</p>
				<a download href="<?php echo $report_pdf ?>"><?php echo $report_label; ?></a>
			</div>
			<?php else: ?>
			<div class="report-wrapper default-label">
			
			<a href="<?php $report_pdf ?>"><?php echo the_title(); ?></a>
			</div>
				<?php endif; ?>
				
				<?php 
			/**
			 * generate_after_main_content hook.
			 *
			 * @since 0.1
			 */
			do_action( 'generate_after_main_content' );
			?>
		</main><!-- #main -->
	</div><!-- #primary -->

	<?php
	/**
	 * generate_after_primary_content_area hook.
	 *
	 * @since 2.0
	 */
	do_action( 'generate_after_primary_content_area' );

	generate_construct_sidebars();

get_footer();

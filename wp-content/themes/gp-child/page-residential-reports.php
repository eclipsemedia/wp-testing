<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package GeneratePress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header(); ?>

	<div id="primary" <?php generate_do_element_classes( 'content' ); ?>>
		<main id="main" <?php generate_do_element_classes( 'main' ); ?>>
    <?php

?>
			<?php
			/**
			 * generate_before_main_content hook.
			 *
			 * @since 0.1
			 */
			do_action( 'generate_before_main_content' );
      $loop = new WP_Query( array(
        'post_type' => 'report',
        'posts_per_page' => 10,
        'tax_query' => array(
          array(
              'taxonomy' => 'custom_tag',   // taxonomy name
              'field' => 'slug',           // term_id, slug or name
              'terms' => 'residential',     // term id, term slug or term name
          )
      )
        )
      );  
			while ( $loop->have_posts() ) : $loop->the_post();

				get_template_part( 'content', 'report' );

					?>
					<?php

			endwhile;

			/**
			 * generate_after_main_content hook.
			 *
			 * @since 0.1
			 */
			do_action( 'generate_after_main_content' );
			?>
		</main><!-- #main -->
	</div><!-- #primary -->

	<?php
	/**
	 * generate_after_primary_content_area hook.
	 *
	 * @since 2.0
	 */
	do_action( 'generate_after_primary_content_area' );

	generate_construct_sidebars();

get_footer();

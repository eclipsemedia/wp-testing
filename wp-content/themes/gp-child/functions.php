<?php


add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );

function enqueue_parent_styles() {
   wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}

//  <!-- custom post types  -->

function my_custom_post_report() {
	$labels = array(
	  'name'               => _x( 'Reports', 'post type general name' ),
	  'singular_name'      => _x( 'Report', 'post type singular name' ),
	  'add_new'            => _x( 'Add New', 'report' ),
	  'add_new_item'       => __( 'Add New report' ),
	  'edit_item'          => __( 'Edit report' ),
	  'new_item'           => __( 'New report' ),
	  'all_items'          => __( 'All reports' ),
	  'view_item'          => __( 'View report' ),
	  'search_items'       => __( 'Search reports' ),
	  'not_found'          => __( 'No reports found' ),
	  'not_found_in_trash' => __( 'No reports found in the Trash' ), 
	  'menu_name'          => 'Reports'
	);
	$args = array(
	  'labels'        => $labels,
	  'description'   => 'Holds our reports and report specific data',
	  'public'        => true,
	  'menu_position' => 5,
	  'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
	  'has_archive'   => true,
	);
	register_post_type( 'report', $args ); 
  }
	add_action( 'init', 'my_custom_post_report' );
	
	// taxonomy for reports post type
	register_taxonomy( 'custom_tag', 
	array('report'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
	array('hierarchical' => true,    /* if this is false, it acts like tags */                
		'labels' => array(
			'name' => __( 'Custom Tags', 'text_domain' ), /* name of the custom taxonomy */
			'singular_name' => __( 'Custom Tag', 'text_domain' ), /* single taxonomy name */
			'search_items' =>  __( 'Search Custom Tags', 'text_domain' ), /* search title for taxomony */
			'all_items' => __( 'All Custom Tags', 'text_domain' ), /* all title for taxonomies */
			'parent_item' => __( 'Parent Custom Tag', 'text_domain' ), /* parent title for taxonomy */
			'parent_item_colon' => __( 'Parent Custom Tag:', 'text_domain' ), /* parent taxonomy title */
			'edit_item' => __( 'Edit Custom Tag', 'text_domain' ), /* edit custom taxonomy title */
			'update_item' => __( 'Update Custom Tag', 'text_domain' ), /* update title for taxonomy */
			'add_new_item' => __( 'Add New Custom Tag', 'text_domain' ), /* add new title for taxonomy */
			'new_item_name' => __( 'New Custom Tag Name', 'text_domain' ) /* name title for taxonomy */
		),
		'show_ui' => true,
		'query_var' => true,
	)
);
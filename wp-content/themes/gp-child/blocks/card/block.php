<?php 
        $img = block_value('image');
        $is_bg_img = block_value('is-background-image');
?>
<?php if($is_bg_img): ?>
    <div class="card has-bg-img" style="background-image: url('<?php block_field('image'); ?>')">
    <p class="card-title"><?php block_field('title'); ?></p>
    <p class="card-text"><?php block_field('sub-text'); ?></p>
</div>
<?php else: ?>
    <div class="card">
    <p class="card-title"><?php block_field('title'); ?></p>
    <p class="card-text"><?php block_field('sub-text'); ?></p>
    <img src="<?php block_field('image'); ?>" />
</div>
<?php endif;
?>

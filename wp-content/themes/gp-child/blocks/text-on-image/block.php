<a href="<?php block_field( 'link' ) ?>">
  <div class="text-on-img" style="background-image: url(<?php block_field( 'image' ) ?>)">
    <h3><?php block_field( 'text' ) ?></h3>
  </div>
</a>
<?php $link = block_value('link'); ?>
<div class="hero-container" style="background-image: url('<?php block_field('image'); ?>')">
    <h1 class="hero-header"><?php block_field('headline') ?></h1>
    <p class="hero-subtext"><?php block_field('subtext') ?></p>
    <?php if ($link): ?>
    <a class="hero-btn" href="<?php block_field('link') ?>"><?php block_field('link-text') ?></a>
    <?php endif; ?>
</div>